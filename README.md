# Voting RPC Client Server

kelompok 9 : Voting menggunakan RPC

## Remote Procedure Call (RPC)
Remote Procedure Call (RPC) adalah sebuah metode yang memungkinkan untuk mengakses sebuah prosedur yang berada di komputer lain. Untuk dapat melakukan ini sebuah server harus menyediakan layanan remote procedure. Server membuka socket, lalu menunggu client yang meminta prosedur yang disediakan oleh server.
Client Server merupakan suatu bentuk arsitektur, dimana client adalah perangkat yang menerima pesan lalu akan menampilkan dan menjalankan aplikasi (software komputer) dan server adalah perangkat yang menyediakan dan pengelola aplikasi.
Pada Tugas ini kami membuat suatu sistem untuk melakukan Voting menggunakan RPC. Dimana sebuah server akan menjadi server dari sistem Voting tersebut dan terdapat beberapa client yang dapat mengakses sistem Voting tersebut untuk melakukan sebuah pemilihan yang menjadi sebuah pesan, dimana pesan tersebut akan dikirimkan ke server.
sebagai penjelasan lebih lanjut berikut merupakan hasil running program nya:

- server

![ser](/uploads/cc9cb2893ace80f78cd19213599d5d76/ser.PNG)

Server mulai menjalankan program.

- Tampilan utama client

![Client_1](/uploads/8ee0eacb3a04503e3cf8fd09c0bf73b6/Client_1.PNG)

Client mulai mengakses program yang terdapat di server. Client mendapatkan 3 menu yang dapat dipilih diantaranya mulai memvoting, melihat hasil voting, dan keluar program. Jika client memilih salah satu menu tersebut maka pesan dari client akan dikirimkan ke server. Client server saling terhubung dengan menggunakan IP. Semua Client mengakses IP Server.



Client memilih menu voting komedian atau film lalu client melakukan pemilihan/voting

![voting_komedian](/uploads/f455c560084b2e3f299e6c69feafe8cc/voting_komedian.PNG)
![voting_film](/uploads/c16cf0482e2df3efdc3ac501a899618c/voting_film.PNG)



- client cek voting atau cek voting persen

![cek_Voting](/uploads/bef551bab5556a8bd3a135fd2cc3ffaf/cek_Voting.PNG)

![cek_Voting_persen](/uploads/a7ea39e5a24949b9cad131acbf6869e8/cek_Voting_persen.PNG)

- fitur total

menampilkan total pemilih pada setiap voting dan semuanya

![total](/uploads/b4a3a27153eeec2e4b350630c7fba45e/total.PNG)

![7](/uploads/153c1c41c9b3724afd7e5f9764191ee7/7.jpg)
server mencatat seluruh log dari client.

Ini merupakan penjelasan singkat mengenai program Voting menggunakan RPC.