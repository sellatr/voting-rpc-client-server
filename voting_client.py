import xmlrpc.client;
s = xmlrpc.client.ServerProxy('http://localhost:8022');

uname = ['jarjis','ihsan','sella','yanda']
password = ['12','123','1234','12345']

# membuat fungsi mVote
def menuVoting():
	print("PILIH VOTING")
	print("1. Voting Komedian Terbaik")
	print("2. Voting Film Terbaik 2019	")

	pil = input("Pilihan : ")
	if(pil == '1'):
		mVote()
	elif(pil == '2'):
		mVoteFilm()
	else:
		print('Pilihan ditolak, Masukkan Lagi\n')
		menuVoting()

# membuat fungsi mVote
def mVote():
	print("=========KANDIDAT KOMEDIAN FAVORIT TERBAIK========")
	print("1. Komeng	")
	print("2. Bolot		")
	print("3. Sule		")
	print("4. Nunung	")
	print("5. Keluar	")
	print("==================================================")

	pilihan = input("pilih komedian favorit anda : ")

	if(pilihan=='1'):
		# memanggil fungsi vote("nama_kandidat") yang ada di server
		print(s.vote("komeng"));
		ask()

	elif(pilihan=='2'):
		# memanggil fungsi vote("nama_kandidat") yang ada di server
		print(s.vote("bolot"));
		ask()
			
	elif(pilihan=='3'):
		# memanggil fungsi vote("nama_kandidat") yang ada di server
		print(s.vote("sule"));
		ask()
			
	elif(pilihan=='4'):
		# memanggil fungsi vote("nama_kandidat") yang ada di server
		print(s.vote("nunung"));
		ask()
		
def mVoteFilm():
	print("======== FILM FAVORIT TERBAIK ========")
	print("1. Avangers : End Game")
	print("2. Pokemon : Detective Pikachu")
	print("3. Dilan 1991")
	print("4. Ada Apa Dengan Cinta (AADC)")
	print("5. Keluar")
	print("==================================================")

	pilihan = input("Pilih film favorit anda : ")

	if(pilihan=='1'):
		# memanggil fungsi vote("nama_kandidat") yang ada di server
		print(s.film("Avangers : End Game"));
		ask()

	elif(pilihan=='2'):
		# memanggil fungsi vote("nama_kandidat") yang ada di server
		print(s.film("Pokemon : Detective Pikachu"));
		ask()
			
	elif(pilihan=='3'):
		# memanggil fungsi vote("nama_kandidat") yang ada di server
		print(s.film("Dilan 1991"));
		ask()
			
	elif(pilihan=='4'):
		# memanggil fungsi vote("nama_kandidat") yang ada di server
		print(s.film("Ada Apa Dengan Cinta (AADC)"));
		ask()
		
def menuCheck():
	print("PILIH VOTING")
	print("1. Hasil Voting Dengan Jumlah")
	print("2. Hasil Voting Dengan Persen (%)")

	pili = input("Pilihan : ")
	if(pili == '1'):
		check()
	elif(pili == '2'):
		checkPer()
	else:
		print('Pilihan ditolak, Masukkan Lagi\n')
		menuCheck()
		
# membuat fungsi check
def check():
	print ("==========HASIL VOTING=======")
	# memanggil fungsi querry() untuk mengetahui hasil voting dari masing-masing kandidat
	candidates = s.querry()		
	for x in candidates:
		print(x+" : "+candidates[x]);
	
	print ("\nHASIL VOTING FILM =======\n")
	# memanggil fungsi querry() untuk mengetahui hasil voting dari masing-masing kandidat
	candFilm = s.hasilfilm()		
	for y in candFilm:
		print(y+" : "+candFilm[y]);

	print("\n==============================")
	ask = input("Apakah anda ingin kembali ke menu (Y/N)?")
	if ask == 'Y' or ask == 'y':
		menu()
	elif ask == 'N' or ask == 'n':
	    print("Terimakasih sudah menggunakan layanan e-voting");
		exit()
		
def checkPer():
	print ("HASIL VOTING PELAWAK =======\n")
	# memanggil fungsi querry() untuk mengetahui hasil voting dari masing-masing kandidat
	candidates = s.querrypersen()		
	for x in candidates:
		print(x+" : "+candidates[x]);

	print ("\nHASIL VOTING FILM =======\n")
	# memanggil fungsi querry() untuk mengetahui hasil voting dari masing-masing kandidat
	candFilm = s.hasilfilmpersen()		
	for y in candFilm:
		print(y+" : "+candFilm[y]);

	print("\n==============================")
	ask = input("Apakah anda ingin kembali ke menu (Y/N)?")
	if ask == 'Y' or ask == 'y':
		menu()
	elif ask == 'N' or ask == 'n':
	    print("Terimakasih sudah menggunakan layanan e-voting");
		exit()
		
def totalVote():
	print ("\nTOTAL VOTERS =======\n")
	candidates = s.tkomedi()	
	for a in candidates:
		q = int(candidates['komeng'])
		w = int(candidates['bolot'])
		e = int(candidates['sule'])
		r = int(candidates['nunung'])
		jumlah = q + w + e + r
	print ("Total Voters Komedian Terbaik : ", jumlah)
	candFilm = s.tfilm()	
	for b in candFilm:
		u = int(candFilm['Avangers : End Game'])
		i = int(candFilm['Pokemon : Detective Pikachu'])
		o = int(candFilm['Dilan 1991'])
		p = int(candFilm['Ada Apa Dengan Cinta (AADC)'])
		jumlahFilm = u + i + o + p

	print ("Total Voters Film Terbaik 2019 : ", jumlahFilm)
	total = jumlah + jumlahFilm
	print ("\nTotal Voters Keseluruhan : ", total)


#membuat fungsi ask
def ask():
	print("==============================")
	ask = input("Apakah anda ingin kembali ke menu (Y/N)?")
	if ask =='Y' or ask =='y':
		menu()
	elif ask =='N' or ask =='n':
		print("==============================")
		print("Terima kasih sudah berpartisipasi")
		print("==============================")

	
# membuat fungsi menu
def menu():
	pilihan=0	
	while pilihan!=3:
		print()
		print("========= VOTING 2019 ========")
		print("1. Voting")
		print("2. Cek Voting")
		print("3. Total Voting")
		print("4. Keluar")
		print("==============================")
		pilihan = input("Masukkan pilihan : ")

		if pilihan == '1':
			menuVoting()
		elif pilihan =='2':
			menuCheck()
		elif pilihan =='3':
			totalVote()
		elif pilihan =='4':
		    print("Terimakasih sudah menggunakan layanan e-voting");
			exit()
		else:
			print("\n===============================")
			print("Inputan yang anda masukkan salah")
			ask()

def login():
	print ("======== LOGIN ========")
	username = input('Username : ')
	passwd = input('Password : ')
	if username in uname:
		posisi = uname.index(username)
		if passwd == password[posisi]: 
			print ('\nHalo, Silahkan Masuk %s ' % username)
			menu()
		else:
			print ('\nPassword Salah\n')
			login()
	else:
		print ('\nMaaf username salah. Akses ditolak\n')
		login()
login()