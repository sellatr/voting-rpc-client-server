# import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer;

# import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCRequestHandler;

import threading;

# Batasi hanya pada path /RPC2 saja supaya tidak bisa mengakses path lainnya
class RequestHandler(SimpleXMLRPCRequestHandler):
	rpc_paths = ('/RPC2',);

# Buat server
with SimpleXMLRPCServer(("localhost",8022),requestHandler=RequestHandler,allow_none=True) as server:
	server.register_introspection_functions();

    # buat data struktur dictionary untuk menampung nama_kandidat dan hasil voting
	candidates ={
		"komeng" :0,
		"bolot":0,
		"sule":0,
		"nunung":0
	};
	
	candFilm ={
		"Avangers : End Game":0,
		"Pokemon : Detective Pikachu":0,
		"Dilan 1991":0,
		"Ada Apa Dengan Cinta (AADC)":0
	}
    
    # kode setelah ini adalah critical section, menambahkan vote tidak boeh terjadi race condition
    # siapkan lock
	lock = threading.Lock();
    
    #  buat fungsi bernama vote_candidate()
	def vote_candidate(x):
        
        # critical section dimulai harus dilock
		lock.acquire();
        # jika kandidat ada dalam dictionary maka tambahkan  nilai votenya
		if x in candidates:
			candidates[x] += 1;
			lock.release();
			return "anda sudah memilih "+x;
        
        # critical section berakhir, harus diunlock
		lock.release();
        
    
    # register fungsi vote_candidate() sebagai vote
	server.register_function(vote_candidate, 'vote');
	
	def vote_candidateFilm(y):        
		lock.acquire();        
		if y in candFilm:
			candFilm[y] += 1;
			lock.release();
			return "Anda sudah memilih "+y;
                
		lock.release();
                
	server.register_function(vote_candidateFilm, 'film');

    # buat fungsi bernama querry_result
	def querry_result():
        # critical section dimulai
		lock.acquire();
        
        # hitung total vote yang ada
		total = 0;
		cdd = {};
		for x in candidates:
			total = total + candidates[x];
        # hitung hasil persentase masing-masing kandidat
		for x in candidates:
			if total!=0:
				per = candidates[x];
				cdd[x] = str(per)+" voters";
        
        # critical section berakhir
		lock.release();
		return cdd;
        
    # register querry_result sebagai querry
	server.register_function(querry_result, 'querry');
	
	def querry_resultFilm():        
		lock.acquire();
                
		totalFilm = 0;
		cddFilm = {};
		for y in candFilm:
			totalFilm = totalFilm + candFilm[y];        
		for y in candFilm:
			if totalFilm!=0:
				perF = candFilm[y];
				cddFilm[y] = str(perF)+" voters";
                
		lock.release();
		return cddFilm;
            
	server.register_function(querry_resultFilm, 'hasilfilm');

	def querry_result_per():        
		lock.acquire();
                
		totalPer = 0;
		cddPer = {};
		for x in candidates:
			totalPer = totalPer + candidates[x];        
		for x in candidates:
			if totalPer!=0:
				perP = candidates[x]/totalPer*100;
				cddPer[x] = str(perP)+"%";
                
		lock.release();
		return cddPer;
            
	server.register_function(querry_result_per, 'querrypersen');

	def querry_resultFilm_per():        
		lock.acquire();
                
		totalFilmP = 0;
		cddFilm = {};
		for y in candFilm:
			totalFilmP = totalFilmP + candFilm[y];        
		for y in candFilm:
			if totalFilmP!=0:
				perFP = candFilm[y]/totalFilmP*100;
				cddFilm[y] = str(perFP)+"%";
                
		lock.release();
		return cddFilm;
            
	server.register_function(querry_resultFilm_per, 'hasilfilmpersen');
	
	def totalKomedian():
		lock.acquire();
                
		total = 0;
		cddTotal = {};
		for a in candidates:
			total = total + candidates[a];         
		for a in candidates:
			if total!=0:
				perTot = candidates[a];
				cddTotal[a] = int(perTot);
                
		lock.release();
		return cddTotal;
            
	server.register_function(totalKomedian, 'tkomedi');

	def totalFilm():        
		lock.acquire();
                
		totalFilm = 0;
		cddTotalF = {};
		for b in candFilm:
			totalFilm = totalFilm + candFilm[b];        
		for b in candFilm:
			if totalFilm!=0:
				perF = candFilm[b];
				cddTotalF[b] = int(perF);
                
		lock.release();
		return cddTotalF;
            
	server.register_function(totalFilm, 'tfilm');


	print ("Server voting berjalan...");
    # Jalankan server
	server.serve_forever();

